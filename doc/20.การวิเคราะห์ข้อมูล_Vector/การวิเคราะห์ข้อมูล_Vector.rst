.. _`การวิเคราะห์ข้อมูล Vector`:

20.	การวิเคราะห์ข้อมูล Vector
================================================

**20.1	คำสั่ง Intersect**

เป็นการซ้อนทับ (Overlay) ข้อมูลระหว่าง 2 ข้อมูล โดยชั้นข้อมูลผลลัพธ์จะเป็นข้อมูลที่อยู่ทั้งขอบเขตพื้นที่ของทั้ง 2 ชั้นข้อมูล ซึ่งจะไม่มีขอบเขตเกินจากข้อมูลทั้ง 2 ชั้น 

.. figure:: img/20_1.png
   :align: center 

**วิธีการ**

1. เปิดชั้นข้อมูล MenangPhuket กับ Village จากโฟลเดอร์ Lab\Geoprocessing\DATA
2. ใช้คำสั่ง Intersect โดยเลือกที่ Vector >> Geoprocessing Tools >> Intersect ==

**การตั้งค่าคำสั่ง**

.. figure:: img/20_2.PNG
   :align: center

3. ผลลัพธ์ของการวิเคราะห์

**ก่อนการวิเคราะห์**

.. figure:: img/20_3.png
   :align: center  

**หลังการวิเคราะห์**

.. figure:: img/20_4.png
   :align: center 

**20.2	คำสั่ง Union**

สนใจพื้นที่ของวัตถุที่ซ้อนกันมากกว่า 2 พื้นที่โดยรวม Feature จำนวน 2 พื้นที่ขึ้นไปเข้าไว้ด้วยกัน และสร้างเป็น Feature ชุดใหม่

.. figure:: img/20_5.png
   :align: center
 

**วิธีการ**

1. เปิดชั้นข้อมูล MenangPhuket กับ District_No_MeangPhuket จากโฟลเดอร์ Lab\Geoprocessing\DATA
2. ใช้คำสั่ง Union โดยเลือกที่ Vector >> Geoprocessing Tools >> Union 

**การตั้งค่าคำสั่ง**

•	Input vector layer คือข้อมูลที่ต้องการนำมา Union
•	Intersect layer คือข้อมูลขอบเขตพื้นที่สำหรับการรวมกับชั้นข้อมูล Input vector layer
•	Union ให้เลือกที่เก็บข้อมูลไปที่ Lab\Geoprocessing\AnalystVector และตั้งชื่อไฟล์ Union สำหรับการจัดเก็บข้อมูล

.. figure:: img/20_6.png
   :align: center 
 
3. ผลลัพธ์ของการวิเคราะห์

**ข้อมูลก่อนการวิเคราะห์**
 
.. figure:: img/20_7.png
   :align: center 

**ผลลัพธ์ที่ได้จากการวิเคราะห์**
 
.. figure:: img/20_8.png
   :align: center  

**20.3	คำสั่ง Clip**

เป็นฟังก์ชันสำหรับตัดข้อมูลที่ต้องการตามขอบเขตที่กำหนด โดยการตัดข้อมูลออกจากข้อมูลที่ต้องการ (Input feature) ด้วยพื้นที่ที่เป็นขอบตัด (Clip feature)

.. figure:: img/20_9.png
   :align: center  
 
**วิธีการ**

1. เปิดชั้นข้อมูล MenangPhuket กับ Road จากโฟลเดอร์ Lab\Geoprocessing\DATA
2. ใช้คำสั่ง Clip โดยเลือกที่ Vector >> Geoprocessing Tools >> Clip  

**การตั้งค่าคำสั่ง**

•	Input vector layer คือข้อมูลที่ต้องการนำมาตัดตามขอบเขต
•	Intersect layer คือข้อมูลขอบเขตพื้นที่
•	Clipped ให้เลือกที่เก็บข้อมูลไปที่ Lab\Geoprocessing\AnalystVector และตั้งชื่อไฟล์ Clip สำหรับการจัดเก็บข้อมูล

.. figure:: img/20_10.png
   :align: center   
 
3. ผลลัพธ์ของการวิเคราะห์

**ข้อมูลก่อนการวิเคราะห์**

.. figure:: img/20_11.png
   :align: center    

**ผลลัพธ์ที่ได้จากการวิเคราะห์**
 
.. figure:: img/20_12.png
   :align: center   

**20.4	คำสั่ง Buffer**

เป็นการสร้างระยะทางที่ห่างจากฟีเจอร์ตามค่าที่กำหนดหรือใช้ค่าจากฟิลด์ การสร้าง Buffer เป็นการวิเคราะห์พื้นที่เพียง 1 ชั้นข้อมูล และเป็นการสร้างพื้นที่ล้อมรอบฟีเจอร์ (Point, Line, Polygon) ของชั้นข้อมูลที่เลือกหรือที่ได้คัดเลือกไว้บางส่วนของข้อมูล ผลที่ได้ข้อมูลใหม่ที่มีขนาดความกว้างของพื้นที่จากตำแหน่งที่เลือก เท่ากับขนาดของ Buffer ที่กำหนดและมีหน่วยตามที่ต้องการ

.. figure:: img/20_13.png
   :align: center   

**วิธีการ**

**กรณีการสร้าง buffer จากระยะทางของข้อมูลใน Field**

1. เปิดชั้นข้อมูล Village จากโฟลเดอร์ Lab\Geoprocessing\DATA
2. ใช้คำสั่ง โดยเลือกที่ Vector >> Geoprocessing Tools >> Varible distance buffer

**การตั้งค่าคำสั่ง**

•	Input vector layer คือข้อมูลที่ต้องการสร้างแนวกันชน
•	Distance field คือ คอลัมน์ระยะทางที่ใช้สำหรับสร้างระยะแนวกันชน
•	Segmente to approximate คือ จำนวนของจุดที่นำมาใช้สร้างเป็นข้อมูลวงกลม
•	กรณีต้องการ Dissovle ผลลัพธ์ให้เลือกตรง  |logo|
•	Buffer ให้เลือกที่เก็บข้อมูลไปที่ Lab\Geoprocessing\AnalystVector และตั้งชื่อไฟล์ Buffer สำหรับการจัดเก็บข้อมูล

.. figure:: img/20_14.png
   :align: center 

3. ผลลัพธ์ของการวิเคราะห์

**ข้อมูลก่อนการวิเคราะห์**

.. figure:: img/20_15.png
   :align: center  

**ผลลัพธ์ที่ได้จากการวิเคราะห์**
 
.. figure:: img/20_16.png
   :align: center 
    
**กรณีการสร้าง buffer โดยการระบุระยะทาง**

1. เปิดชั้นข้อมูล Village จากโฟลเดอร์ Lab\Geoprocessing\DATA
2. ใช้คำสั่ง โดยเลือกที่ Processing >> Processing Toobox >> Geoprocessing Tools >> Vector geometry >>Buffer

**การตั้งค่าคำสั่ง**

•	Input vector layer คือข้อมูลที่ต้องการสร้างแนวกันชน
•	Distance คือ ระยะของแนวกันชน กำหนดเท่ากับ 500 เมตร
•	Segmente to approximate คือ จำนวนของจุดที่นำมาใช้สร้างเป็นข้อมูลวงกลม
•	กรณีต้องการ Dissovle ผลลัพธ์ให้เลือกตรง  |logo|
•	Buffered ให้เลือกที่เก็บข้อมูลไปที่ Lab\Geoprocessing\AnalystVector และตั้งชื่อไฟล์ BufferDistance สำหรับการจัดเก็บข้อมูล

.. |logo| image:: img/20_19.png

.. figure:: img/20_17.png
   :align: center

**ผลลัพธ์ที่ได้จากการวิเคราะห์**

.. figure:: img/20_18.png
   :align: center 

**ผลลัพธ์ที่ได้จากการวิเคราะห์ กรณีสั่ง Dissolve**
 
.. figure:: img/20_20.png
   :align: center

**20.5	คำสั่ง Symmetrical difference**

เป็นคำสั่งที่ใช้สร้างของข้อมูลที่อยู่ด้านนอกขอบเขตข้อมูล difference โดยข้อมูลที่ Input vector จะถูกลบด้วยขอบเขตของข้อมูล คล้ายกับกำร Clip แต่การ difference จะเหลือข้อมูลที่อยู่นอกข้อมูล difference
 	
.. figure:: img/20_21.png
   :align: center

**วิธีการ**

1. เปิดชั้นข้อมูล Polygon1 กับ Polygon2 จากโฟลเดอร์ Lab\Geoprocessing\DATA
2. ใช้คำสั่ง โดยเลือกที่ Vector >> Geoprocessing Tools >> Symmetrical difference 

**การตั้งค่าคำสั่ง**

•	Input vector layer คือข้อมูลที่ต้องการสร้างขอบเขตพื้นที่
•	Difference layer คือข้อมูลพื้นที่ที่ต้องการลบออก
•	Symmetrical difference ให้เลือกที่เก็บข้อมูลไปที่ Lab\Geoprocessing\AnalystVector และตั้งชื่อไฟล์ Sy_diff สำหรับการจัดเก็บข้อมูล

.. figure:: img/20_22.png
   :align: center 
 
3. ผลลัพธ์ของการวิเคราะห์

**ข้อมูลก่อนการวิเคราะห์**

.. figure:: img/20_23.png
   :align: center 

**ผลลัพธ์ที่ได้จากการวิเคราะห์**

.. figure:: img/20_24.png
   :align: center 
 
**20.6.	คำสั่ง Difference**

เป็นคำสั่งที่ใช้สร้างขอบเขตของข้อมูลที่ใหม่ โดยข้อมูลที่ Input vector จะถูกลบด้วยขอบเขตของข้อมูลกับข้อมูลที่นำมา difference


**วิธีการ**

1. เปิดชั้นข้อมูล Polygon1 กับ Polygon3 จากโฟลเดอร์ Lab\Geoprocessing\DATA
2. ใช้คำสั่ง  โดยเลือกที่ Vector >> Geoprocessing Tools >> Symmetrical difference 

**การตั้งค่าคำสั่ง**

• Input vector layer คือข้อมูลที่ต้องการสร้างขอบเขตพื้นที่ใหม่
• Difference layer คือข้อมูลพื้นที่ที่ต้องการนำมาลบกับขอบเขตพื้นที่ของ Input vector layer
• Output shapefile ให้เลือกที่เก็บข้อมูลไปที่ Lab\Geoprocessing\AnalystVector และตั้งชื่อไฟล์ diff สำหรับการจัดเก็บข้อมูล
 
.. figure:: img/20_25.png
   :align: center 
    
3. ผลลัพธ์ของการวิเคราะห์

**ข้อมูลก่อนการวิเคราะห์**

.. figure:: img/20_26.png
   :align: center  

**ผลลัพธ์ที่ได้จากการวิเคราะห์**
 
.. figure:: img/20_27.png
   :align: center  

**20.7.	คำสั่ง Eliminate Selected Polygon**

เป็นคำสั่งที่ใช้สร้างขอบเขตของข้อมูลที่ใหม่ โดยข้อมูลที่ถูกเลือกไว้จะถูกรวมกับข้อมูลที่อยู่ใกล้เคียงโดยพิจารณาข้อมูลใกล้เคียงที่มีพื้นที่ติดต่อกับข้อมูลดังกล่าวมากที่สุด 

**วิธีการ**

1. เปิดชั้นข้อมูล District จากโฟลเดอร์ Lab\Geoprocessing\DATA
2. Selection ข้อมูล พื้นที่ที่ต้องการ
3. Merge selection with the neighbouring polygon with the ให้เลือกแบบ Largest Area(พื้นที่ขนาดใหญ่)
4. ใช้คำสั่ง  โดยเลือกที่ Vector >> Geoprocessing Tools >> Eliminate sliver polygon 

**การตั้งค่าคำสั่ง**

• Input vector layer ชั้นข้อมูล Polygon ที่มีการเลือกข้อมูลในคอลัมน์แล้ว
• Output shapefile ให้เลือกที่เก็บข้อมูลไปที่ Lab\Geoprocessing\AnalystVector และตั้งชื่อไฟล์ Eliminate 

.. figure:: img/20_28.png
   :align: center   

 
3. ผลลัพธ์ของการวิเคราะห์

**ข้อมูลก่อนการวิเคราะห์**

.. figure:: img/20_29.png
   :align: center   

**ผลลัพธ์ที่ได้จากการวิเคราะห์**

.. figure:: img/20_30.png
   :align: center   
 
**20.8	คำสั่ง Dissolve**

คำสั่งนี้ใช้สำหรับรวมกลุ่มข้อมูลพื้นที่ที่มีคุณสมบัติหรือค่าตารางที่เหมือนกันที่เข้าด้วยกัน เพื่อลดความซ้ำซ้อนของชั้นข้อมูลให้น้อยลง ซึ่งเป็นการนำเส้นขอบเขตของพื้นที่ที่มีค่าเหมือนกันในหนึ่งหรือหลายฟิลด์ออกไป

.. figure:: img/20_31.png
   :align: center  

**วิธีการ**

1. เปิดชั้นข้อมูล MeangPhuket จากโฟลเดอร์ Lab\Geoprocessing\DATA
2. ใช้คำสั่ง โดยเลือกที่ Processing >> Processing Toolbox >> Vector geometry>> Dissolve

**การตั้งค่าคำสั่ง**

•	Input vector layer คือข้อมูลที่ต้องการรวมพื้นที่เข้าด้วยกัน
•	Unique ID field คือ คอลัมน์ที่ต้องการนำมา รวมพื้นที่เข้าด้วยกัน ให้เลือก Select all สำหรับรวมพื้นที่ให้เป็นพื้นที่เดียวกัน
•	Dissolve ให้เลือกที่เก็บข้อมูลไปที่ Lab\Geoprocessing\AnalystVector และตั้งชื่อไฟล์ Dissolve สำหรับการจัดเก็บข้อมูล

.. figure:: img/20_32.png
   :align: center  
     
3. ผลลัพธ์ของการวิเคราะห์ 

**ข้อมูลก่อนการวิเคราะห์**

.. figure:: img/20_33.png
   :align: center  

**ผลลัพธ์ที่ได้จากการวิเคราะห์**
 
.. figure:: img/20_34.png
   :align: center  

**20.9	คำสั่ง Convex Hull**

เป็นคำสั่งสำหรับการสร้างข้อมูลพื้นที่ข้างนอกให้นูนออกไปจากชั้นข้อมูลเดิมที่มีอยู่  โดยข้อมูลที่ได้จะมีจำนวน Node ของข้อมูลที่ลดลง แต่เส้นขอบรูปร่างอาจจะมีการแปลงแปลงไป

**วิธีการ**

1. เปิดชั้นข้อมูล MeangPhuket จากโฟลเดอร์ Lab\Geoprocessing\DATA
2. ใช้คำสั่ง  โดยเลือกที่ Vector >> Geoprocessing Tools >> Convex hull 

**การตั้งค่าคำสั่ง**

• Input vector layer คือข้อมูลที่ต้องการส้รางพื้นที่
• Create single minimum convex hull 
• Output shapefile ให้เลือกที่เก็บข้อมูลไปที่ Lab\Geoprocessing\AnalystVector และตั้งชื่อไฟล์ Convex สำหรับการจัดเก็บข้อมูล
 
.. figure:: img/20_35.png
   :align: center  
    
3. ผลลัพธ์ของการวิเคราะห์

**ข้อมูลก่อนการวิเคราะห์**

.. figure:: img/20_36.png
   :align: center 

**ผลลัพธ์ที่ได้จากการวิเคราะห์**
 
.. figure:: img/20_37.png
   :align: center 

**20.10	คำสั่ง Merge**

เป็นการรวมฟีเจอร์จากหลายชั้นข้อมูลเข้าเป็นชั้นข้อมูลเดียวกัน คำสั่ง Merge สามารถใช้ได้ทั้งข้อมูลเป็น Point, Line และ Polygon เพื่อเป็นการเชื่อมต่อแผนที่ที่มีระบบพิกัดภูมิศาสตร์อยู่ในพื้นที่ใกล้เคียงกันหรือต่อกัน ผลลัพธ์ที่ได้จะสร้างชั้นข้อมูลใหม่ที่มีอยู่ต่อกัน 
 
.. figure:: img/20_38.png
   :align: center 

**วิธีการ**

1. เปิดชั้นข้อมูลจากโฟลเดอร์ Lab\Geoprocessing\AnalystVector\Split
2. ใช้คำสั่ง โดยเลือกที่ Processing >> Processing Toolbox >> Vector geometry>> Merge Shapefile vector layer 

**การตั้งค่าคำสั่ง**

•	Input layers คือการกำหนดชั้นข้อมูลที่ต้องการ Merge กำหนดให้เลือกตั้งหมด
•	Destination CES คือการกำหนดพิกัดใหม่ให้กับข้อมูล
•	Merge ให้เลือกที่เก็บข้อมูลไปที่ Lab\Geoprocessing\AnalystVector >> Split   และตั้งชื่อ Merge สำหรับการจัดเก็บข้อมูล

.. figure:: img/20_39.png
   :align: center  

3. ผลลัพธ์ของการวิเคราะห์

**ข้อมูลก่อนการวิเคราะห์**

.. figure:: img/20_40.png
   :align: center  

**ผลลัพธ์ที่ได้จากการวิเคราะห์**

.. figure:: img/20_41.png
   :align: center 
