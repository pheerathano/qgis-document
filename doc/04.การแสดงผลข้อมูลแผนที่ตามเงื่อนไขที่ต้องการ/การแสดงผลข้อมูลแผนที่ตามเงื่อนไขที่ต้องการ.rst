.. _`การแสดงผลข้อมูลแผนที่ตามเงื่อนไขที่ต้องการ`:

4.	การแสดงผลข้อมูลแผนที่ตามเงื่อนไขที่ต้องการ
=====================

**4.1.	การแสดงผลข้อมูลตามขนาดของข้อมูล**

**วิธีการ**

1. เปิดชั้นข้อมูล BKK_branch จากโฟลเดอร์ Lab/DataForKTB/UTM
2. คลิกขวาที่ชั้นข้อมูลเลือก Properties
3. เลือกคำสั่ง Symbology >> Graduated
4. Method เท่ากับ Size

.. _figure_modeler:

.. figure:: img/4_1.png
   :width: 320px
   :align: center
   :height: 190px
   :alt: alternate text
 
**ผลลัพธ์**

.. _figure_modeler:

.. figure:: img/4_2.png
   :width: 320px
   :align: center
   :height: 190px
   :alt: alternate text

**4.2.	การแสดงผลข้อมูลจากตารางข้อมูลประชากร**

**วิธีการ**

1. เปิดชั้นข้อมูล Bangkok จากโฟลเดอร์ Lab/Vector
2. คลิกขวาที่ชั้นข้อมูลเลือก Properties
3. เลือกคำสั่ง Symbology >> Graduated
4. Method เท่ากับ Color

.. _figure_modeler:

.. figure:: img/4_3.png
   :width: 320px
   :align: center
   :height: 190px
   :alt: alternate text
 
**ผลลัพธ์**
 
.. _figure_modeler:

.. figure:: img/4_4.png
   :width: 320px
   :align: center
   :height: 190px
   :alt: alternate text
