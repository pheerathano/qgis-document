.. _`การนำเข้าไฟล์ข้อมูล Excel`:

12.	การนำเข้าไฟล์ข้อมูล Excel
========================

**12.1	การบันทึกไฟล์ข้อมูล Excel สำหรับใช้ในโปรแกรม QGIS**

ข้อมูลที่จัดเก็บอยู่รูปแบบของ Excel ข้อมูลต้องมีคอลัมน์ระบุพิกัดข้อมูลของข้อมูลนั้น ๆ โปรแกรม QGIS รองรับการใช้งานไฟล์แบบ CSV (Comma delimited)(*.csv)

**วิธีการ**

1. เปิดโปรแกรม Excel Data จากโฟลเดอร์ Lab/DataForKTB

   .. _figure_modeler:

   .. figure:: img/12_1.png
      :align: center 

2. บันทึกไฟล์ (Save As) ไฟล์ เป็น CSV (Comma delimited) (*.csv)

   .. _figure_modeler:

   .. figure:: img/12_2.png
      :align: center  

 
**12.2	การนำเข้าไฟล์ข้อมูล Excel (Import Excel)**

**วิธีการ**

1. การนำเข้าไฟล์ข้อมูล CSV ใช้คำสั่ง Add Delimited Text Layer   
2. ปรับค่าตามรูปแบบของข้อมูล โดยเลือก Geometry definition และกำหนดค่า Geometry CRS เลือกระบบพิกัดให้กับข้อมูล ถ้าข้อมูลเป็นแบบ ละติจูด ลองจิจูด ให้เลือก WGS 84 
3. เมื่อกำหนดค่าต่าง ๆ เสร็จแล้วให้กดปุ่ม Add

   .. _figure_modeler:

   .. figure:: img/12_4.png
      :align: center

.. |logo12_3| image:: img/12_3.png
    :width: 25pt
    :height: 25pt

จุดพิกัดของสถานที่ก็จะปรากฏบนหน้าต่างแสดงแผนที่
 
   .. _figure_modeler:

   .. figure:: img/12_5.png
      :align: center
       
**12.3	การบันทึก Shapefile จากไฟล์ Excel**

การบันทึกข้อมูลจากไฟล์ Excel ที่นำเข้ามาให้อยู่ในรูปแบบของ Shapefile สามารถทำได้โดยวิธีการ Save as เป็นข้อมูล Shapefile กำหนดให้เก็บข้อมูลไว้ที่ Lab/DataForKTB ตั้งชื่อไฟล์ BKK_branch

   .. _figure_modeler:

   .. figure:: img/12_6.png
      :align: center 

**Note:** ให้นำเข้าไฟล์ข้อมูลดังต่อไปนี้และให้บันทึกข้อมูลเป็น Shapefile 

•	BKK_ATM
•	UPC_Branch
•	UPC_ATM
