.. _`การค้นหาแหล่งให้บริการข้อมูลทางด้านภูมิสารสนเทศจากแหล่งต่าง ๆ`:

2.	การค้นหาแหล่งให้บริการข้อมูลทางด้านภูมิสารสนเทศจากแหล่งต่าง ๆ
=============================
แหล่งข้อมูลที่สามารถนำข้อมูลเกี่ยวกับสารสนเทศภูมิศาสตร์มาใช้งานในด้านต่าง ๆ  สามารถเข้าไปสืบค้นจากแหล่งข้อมูลต่อไปนี้
Export OpenStreetMap Data
ผู้ใช้งานสามารถดาวน์โหลดข้อมูล OpenStreetMap มาใช้งานได้โดย เข้าไปที่เว็บไซต์ https://www.openstreetmap.org/export#map=5/51.500/-0.100 

วิธีการดาวน์โหลด

1. เลือกเมนู Geofabrik Downloads

.. figure:: img/picture1.png
   :align: center 
 
2. เลือกข้อมูลประเทศที่ต้องการ

.. figure:: img/picture2.png
   :align: center 
  
3. กดปุ่มดาวน์โหลดแบบ [. osm.bz2] 

.. figure:: img/picture3.png
   :align: center 
 
4. ไฟล์ที่ได้จากการดาวน์โหลด

.. figure:: img/picture4.png
   :align: center 
 
5. ข้อมูลที่แสดงในโปรแกรม QGIS

.. figure:: img/picture5.png
   :align: center 
 

 

 





