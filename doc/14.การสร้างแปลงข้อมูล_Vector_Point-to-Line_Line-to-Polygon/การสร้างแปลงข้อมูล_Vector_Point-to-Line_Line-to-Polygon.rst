.. _`การสร้างแปลงข้อมูล Vector (Point to Line , Line to Polygon)`:

14.	การสร้างแปลงข้อมูล Vector (Point to Line , Line to Polygon)
========================

โปรแกรม QGIS การแปลงข้อมูลจุด (Point) ให้เป็นข้อมูลเส้น (Line) หรือแปลงข้อมูลจุดให้เป็นข้อมูลรูปปิด (Polygon) สามารถทำได้ดังนี้แต่แปลงข้อมูลดังกล่าว ลำดับของจุดมีความสำคัญต่อการแปลงข้อมูล คำสั่งจะทำการแปลงข้อมูลโดยเริ่มจากข้อมูลจุดแรกไปยังจุดสุดท้ายตามการเรียงลำดับของข้อมูล  
	
**14.1	Point to line**

1. เปิดไฟล์ข้อมูล Point ในโฟลเดอร์ Point2Polygon
2. เลือกPoint to Path

   • Order Field คือ ลำดับของการสร้างข้อมูล
   • Group field คือ การจัดกลุ่มของข้อมูล
 
.. figure:: img/14_1.png
   :align: center
       
ข้อมูลเส้นที่ได้จากการใช้คำสั่ง 

.. figure:: img/14_2.png
   :align: center 
 
**14.2	Line to Polygon**

1. เปิดไฟล์ข้อมูล Line ในโฟลเดอร์ Point2Polygon
2. เลือก Line to Polygon
 
.. figure:: img/14_3.png
   :align: center
       
ข้อมูลเส้นที่ได้จากการใช้คำสั่ง 

.. figure:: img/14_4.png
   :align: center 
