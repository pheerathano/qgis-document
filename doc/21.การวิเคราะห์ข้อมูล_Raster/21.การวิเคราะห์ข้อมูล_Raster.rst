.. _`การวิเคราะห์ข้อมูล Raster (Terrain Analysis)`:

21. การวิเคราะห์ข้อมูล Raster (Terrain Analysis)
==============================================

**ข้อมูล Raster** 

พื้นที่แบบ raster มีการจัดเก็บด้วยรูปแบบที่เรียกว่า กริด (Grid) โดยที่กริดประกอบขึ้นมาจากรูปสี่เหลี่ยมจัตุรัส ในแต่ละช่องสี่เหลี่ยมเรียกว่าเซลล์ (Cell) ดังนั้นหากขนาดของเซลล์เล็ก กริดนั้นจะเป็นกริดที่มีความละเอียดสูง

**ข้อมูล DEM (Digital Elevation Model)**

คือแบบจำลองระดับสูงเชิงเลข ได้จากการรังวัดความสูงหรือจุดระดับความสูงที่เป็นตัวแทนของภูมิประเทศ มีการจัดเก็บข้อมูล การประมวลผล และการนำเสนอแบบจำลองในรูปแบบต่าง ๆ เช่น การสร้างแบบจำลองสามมิติ (3D) แบบจำลองสามมิติเสมือนจริง

**วิธีเปิดข้อมูล**

1.	เปิดข้อมูลโดยใช้คำสั่ง Add Raster Layer  |logo|
2.	เลือกไปที่โฟลเดอร์ Raster เลือกเปิดไฟล์ DEM.tiff
 
.. |logo| image:: img/icon_1.png

.. image:: img/21_1.png
 
ข้อมูล DEM ที่แสดงในโปรแกรม

.. image:: img/21_2.png
 
.. image:: img/21_3.png
 
การปรับแต่งการแสดงผลข้อมูล Raster

คลิกขวาที่ชั้นข้อมูลเลือกคำสั่ง Properties >> Symbology ปรับค่าการแสดงผลข้อมูลดังนี้

-	Render type = Singleband pseudocolor
-	band = Band 1 (Gray)
-   Color ramp = เลือกสี
-	Classify (แบ่งข้อมูลออกเป็น 5 ชั้นข้อมูล)
-	กดปุ่ม OK
 
.. image:: img/21_4.png
 
**21.1	การตัดข้อมูลราสเตอร์ (Clip Raster)**

**วิธีการ**

1. เลือกคำสั่ง Raster >> Extraction >> Clip Raster by Mask Layer

**กำหนดรายละเอียดดังนี้**

2. Input file (raster) ข้อมูลราสเตอร์ DEM
3. Output file (Clipped (mask)) เลือกที่เก็บข้อมูลไปที่โฟลเดอร์ Raster ตั้งชื่อไฟล์ ClipDEM.tiff
4. Mask layer ใช้ข้อมูล MeangPhuket จากโฟลเดอร์ Geoprocessing/DATA
 
.. image:: img/21_5.png

**21.2	ข้อมูล Slope**

ความลาดชันของพื้นที่มีความสำคัญต่อการสำรวจและการทำแผนที่ชนิดดิน เนื่องจากเมื่อความลาดชันของพื้นที่เปลี่ยนไปทำให้สภาพพื้นที่และชนิดดินเปลี่ยนแปลงตามไปด้วย ความลาดชันมีผลต่ออัตราการเลื่อนไหลของดิน (land slide)
ก่อนที่จะใช้คำสั่ง  Terrain Analysis หรือต้องการเครื่องที่ใช้ในการเว็บเคราะห์ข้อมูลเพิ่มเติมให้ติดตั้ง Plugins เพิ่มดังนี้

•	เลือกคำสั่ง Plugins  >> Manage and install  Plugins >> เลือก  Plugins Terrain Analysis

**วิธีการสร้าง Slope**

1.	เลือกคำสั่ง >> Raster >> Terrain analysis >> Slope
2.	Elevation layer คือข้อมูลที่นำมาสร้าง Slope ให้ใช้ข้อมูล DEMUTM
3.	Output layer เลือกที่เก็บข้อมูลไปที่ TerrainAnalyst ตั้งชื่อไฟล์ Slope 
4.	Z factor เลือกเท่ากับ 1 
 
.. image:: img/21_6.png

ข้อมูล Slope ที่ได้

.. image:: img/21_7.png
 
ปรับแต่งการแสดงผลข้อมูล

.. image:: img/21_8.png
 
**21.3	ข้อมูล hill shade**

เป็นคำสั่งที่ใช้ในการดูพื้นที่ความสูงต่ำของข้อมูล(เนินเขา) ผลลัพธ์ที่ได้ออกมาจะแสดงความสูงต่ำของพื้นที่แบบเนินเขา

**วิธีการสร้าง hill shade**

1. เลือกคำสั่ง >> Raster >> Terrain analysis >> hillshade
2. Elevation layer คือข้อมูลที่นำมาสร้าง hillshade ให้ใช้ข้อมูล DEM
3. Output layer เลือกที่เก็บข้อมูลไปที่ TerrainAnalyst ตั้งชื่อไฟล์ hillshade
4. Z factor เลือกเท่ากับ 1
 
.. image:: img/21_9.png

ข้อมูล hill shade 
 
.. image:: img/21_10.png
 
**21.4	ข้อมูล Aspect**

เป็นคำสั่งที่ใช้สำหรับดูทิศทางความลาดชั้นของพื้นที่ โดยนำข้อมูล DEM มาใช้วิเคราะห์

**วิธีการสร้าง Aspect**

1.	เลือกคำสั่ง >> Raster >> Terrain analysis >> Aspect
2.	Elevation layer คือข้อมูลที่นำมาสร้าง Aspect ให้ใช้ข้อมูล DEM
3.	Output layer (Aspect) เลือกที่เก็บข้อมูลไปที่ TerrainAnalyst ตั้งชื่อไฟล์ Aspect

.. image:: img/21_11.png
 
ข้อมูล Aspect
 
.. image:: img/21_12.png

**21.5	ข้อมูล Contour**

คือเส้นชั้นความสูง (Elevation) แต่ละเส้นจะมีค่าดัชนีเส้นชั้นความสูง (Index Contour Lines) ค่าหนึ่ง

**วิธีการสร้าง Contour**

1. เลือกคำสั่ง >> Raster >> Extraction >> Contour
2. Input file (raster) คือข้อมูลที่นำมาสร้าง Contour ให้ใช้ข้อมูล DEMUTM
3. Output file เลือกที่เก็บข้อมูลไปที่ Contour ตั้งชื่อไฟล์ Contour
4. Interval between contour line เลือกเท่ากับ 10
5. ติ๊ก x Attribute name  ให้ตั้งชื่อ ELEV
 
.. image:: img/21_13.png
 
ข้อมูล Contour ที่ได้

.. image:: img/21_14.png
